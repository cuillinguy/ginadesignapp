// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from 'firebase/app'

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import 'firebase/analytics'

// Add the Firebase products that you want to use
import 'firebase/auth'
import 'firebase/database'
import 'firebase/firestore'
import 'firebase/performance'
// import 'firebase/messaging'

const firebaseConfig = {
    apiKey: 'AIzaSyAyeQEoC2PW9EAitaLy7-xjdA2r2Fn7Cu0',
    authDomain: 'ginadesign-4824a.firebaseapp.com',
    databaseURL: 'https://ginadesign-4824a.firebaseio.com',
    projectId: 'ginadesign-4824a',
    storageBucket: 'ginadesign-4824a.appspot.com',
    messagingSenderId: '243436228613',
    appId: '1:243436228613:web:c5cb1cbb6b4787077e4c6c',
    measurementId: 'G-25YNXBRDH9'
    }
    // Initialize Firebase
    const firebaseApp = firebase.initializeApp(firebaseConfig)

    // Enable offline support
    // https://dev.to/paco_ita/break-the-cache-api-limits-in-our-pwa-oo3
    firebase.firestore().enablePersistence()
    .catch(function (err) {
        // eslint-disable-next-line eqeqeq
        if (err.code == 'unimplemented') {
            // The current browser does not support all of the
            // features required to enable persistence
        }
    })

    const firebaseAuth = firebaseApp.auth()
    const firebaseDb = firebaseApp.database()
    const firestoreDb = firebase.firestore()
    const firebaseRoot = firebase
    const perf = firebase.performance()

    // Messaging for firebase
    // const messaging = firebase.messaging()
    // messaging.requestPermission()
    // .then(function () {
    //     console.log('have messaging permission')
    //     return messaging.getToken()
    // })
    // .then(function (token) {
    //     console.log('this is the client token.')
    //     // This should be logged to the server in real life
    // })
    // .catch(function (err) {
    //     console.log('An error occurred getting client permission ' + err)
    // })

    firebase.analytics()

    export { firebaseAuth, firebaseDb, firestoreDb, firebaseRoot }
