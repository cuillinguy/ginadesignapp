declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: string;
    VUE_ROUTER_MODE: 'hash' | 'history' | 'abstract' | undefined;
    VUE_ROUTER_BASE: string | undefined;
    VUE_APP_STRIPE_PUBLISHABLE_KEY: 'pk_test_8AuI0i9pYw4zSNAm1GZYmbBC00HzTs7x3N';
  }
}
