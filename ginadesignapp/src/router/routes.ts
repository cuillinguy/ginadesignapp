import { RouteConfig } from 'vue-router'

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/Index.vue') },
    { path: '/portfolio', component: () => import('pages/Portfolio.vue') },
    { path: '/about', component: () => import('pages/About.vue') },
    { path: '/contact', component: () => import('pages/Contact.vue') },
    { path: '/shop', component: () => import('pages/Shop.vue') },
    { path: '/terms', component: () => import('pages/Footer/Terms.vue') },
    { path: '/privacy', component: () => import('pages/Footer/Privacy.vue') },
    { path: '/checkout', component: () => import('pages/Orders/Checkout.vue') },
    { path: '/login', component: () => import('pages/Authentication/Login.vue') },
    { path: '/login/cart', component: () => import('pages/Authentication/Login.vue') },
    { path: '/upgrade', component: () => import('pages/Authentication/Upgrade.vue') },
    {
      path: '/profile', 
      component: () => import('pages/LoggedIn/Profile.vue'),
      meta: {
        requiresAuth: false
      } 
    },
     {
       path: '/orderdetails/:orderid', 
       component: () => import('pages/LoggedIn/OrderDetails.vue'),
       meta: {
         requiresAuth: false
       } 
     }
    
  ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
