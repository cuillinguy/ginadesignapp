import * as firebase from 'firebase/app'
import 'firebase/storage'
import 'firebase/database'

// let firebaseApp = null

const config = {
  apiKey: 'AIzaSyAyeQEoC2PW9EAitaLy7-xjdA2r2Fn7Cu0',
  databaseURL: 'https://ginadesign-4824a.firebaseio.com',
  projectId: 'ginadesign-4824a',
  storageBucket: 'ginadesign-4824a.appspot.com'
}

if (!firebase.apps.length) {
 firebase.initializeApp(config)
}

const db = firebase.database()
const storage = firebase.storage()

export { firebase, storage, db }
