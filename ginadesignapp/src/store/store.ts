import Vue from 'vue'
import { firestoreDb, firebaseAuth, firebaseDb, firebaseRoot } from 'boot/firebase'
import { stat } from 'fs'
const state = {
    testdata: [],
    products: {},
    userDetails: {},
    pagedproducts: {
        products: {},
        pagination: {
            page: 1,
            limit: 10,
            totalPages: 0
        }
    },
    basket: {},
    userPermissions: {}
}
const mutations = {
    setUserDetails (state, payload) {
        console.log('set user details fired')
        state.userDetails = payload
    },
    setUserPermissions (state, payload) {
        // state.userPermissions = payload
        console.log('set user permissions fired ' + payload.userPermissions)

        // state.userPermissions = payload
        Vue.set(state.userPermissions, payload.userId, payload.userPermission)
    },
    addProduct (state, payload) {

        Vue.set(state.products, payload.productsku, payload.product)
        // console.log('payload product ' + payload.product)
        // console.log('payload product name ' + payload.product.name)
        console.log('sku ' + payload.product.sku)
    },
    addPagedProduct (state, payload) {
        // Test
        // Add this to the paged product state
        Vue.set(state.pagedproducts.products, payload.productsSku, payload.product)
    },
    updateProduct (state, payload) {
        console.log('Update fired')
        Vue.set(state.products, payload.productsku, payload.product)
    },
    addToBasket (state, payload) {

        Vue.set(state.basket, payload.orderitem.sku, payload.orderitem)
    },
    deleteProductFromBasket (state, sku) {

        Vue.delete(state.basket, sku)
    },
    removeUserDetails (state) {
        state.userDetails = {}
    },
    removeUserPermissions (state) {
        state.userPermissions = {}
    },
    clearBasket (state) {
        state.basket = {}
    }
}
const actions = {
    getProducts ({ commit }) {
        const productsListener = firestoreDb
        .collection('Shop')
        .onSnapshot(result => {
            const changes = result.docChanges()

            changes.forEach(change => {

                const product = {
                    name: '',
                    image: '',
                    imagelarge: '',
                    featured: '',
                    sku: '',
                    price: '',
                    thumbnail: '',
                    category: '',
                    issingleitem: '',
                    priceper: '',
                    stock: ''
                }
                    product.name = change.doc.data().name
                    product.image = change.doc.data().image
                    product.imagelarge = change.doc.data().imagelarge
                    product.featured = change.doc.data().featured
                    product.sku = change.doc.data().SKU
                    product.price = change.doc.data().price
                    product.thumbnail = change.doc.data().thumbnail
                    product.category = change.doc.data().category
                    product.issingleitem = change.doc.data().issingleitem
                    product.priceper = change.doc.data().priceper
                    product.stock = change.doc.data().stock
                    const productsku = change.doc.data().SKU

                if (change.type === 'added') {
                    
                    commit('addProduct', {
                        productsku,
                        product
                    })
                } 
                if (change.type === 'modified') {

                    commit('updateProduct', {
                        productsku,
                        product
                    })
                }
            })
        })
    },
    // getProductsPaged ({ commit, page }) {
    //     const productsListener = firestoreDb
    //     .collection('Shop')
    //     .limit(2)
    //     .onSnapshot(result => {
    //         const changes = result.docChanges()

    //         changes.forEach(change => {

    //             const product = {
    //                 name: '',
    //                 image: '',
    //                 imagelarge: '',
    //                 featured: '',
    //                 sku: '',
    //                 price: '',
    //                 thumbnail: '',
    //                 category: '',
    //                 issingleitem: '',
    //                 priceper: '',
    //                 stock: ''
    //             }
    //                 product.name = change.doc.data().name
    //                 product.image = change.doc.data().image
    //                 product.imagelarge = change.doc.data().imagelarge
    //                 product.featured = change.doc.data().featured
    //                 product.sku = change.doc.data().SKU
    //                 product.price = change.doc.data().price
    //                 product.thumbnail = change.doc.data().thumbnail
    //                 product.category = change.doc.data().category
    //                 product.issingleitem = change.doc.data().issingleitem
    //                 product.priceper = change.doc.data().priceper
    //                 product.stock = change.doc.data().stock
    //                 const productsku = change.doc.data().SKU

    //             if (change.type === 'added') {
                    
    //                 commit('addProduct', {
    //                     productsku,
    //                     product
    //                 })
    //             } 
    //             if (change.type === 'modified') {

    //                 commit('updateProduct', {
    //                     productsku,
    //                     product
    //                 })
    //             }
    //         })
    //     })
    // },
    getProductDetails ({ commit, state }, productSku) {
        const productItem = {}
            Object.keys(state.products).forEach(key => {
                if (key === productSku) {
                    productItem[key] = state.products[key]
                    console.log('product is found')
                }
                // console.log('product key ' + key)
                // console.log('state.products[key]' + state.products[key])
                // console.log('productsSku ' + productSku)
            })
        // console.log('fired getProductDetails')
        return productItem
    },
    addOrderToBasket ({ commit, state }, order) {
        // Are they logged in?

        const orderitem = {
            sku: '',
            amount: '',
            name: '',
            userid: '',
            price: '',
            ordertotal: ''
        }

        orderitem.sku = order.sku
        orderitem.amount = order.amount
        orderitem.name = order.name
        orderitem.userid = order.userid
        orderitem.price = order.price
        orderitem.ordertotal = order.orderTotal
        const userid = order.userid
        const payload = {
            userid: userid,
            orderitem: orderitem
        }
        commit('addToBasket', payload)
    },
    removeProductFromBasket ({ commit }, sku) {
        console.log('sku removed ' + sku)
        commit('deleteProductFromBasket', sku)
    },
    clearBasket ({ commit }) {
        commit('clearBasket')
    },
    addAnonymousUser () {
        firebaseAuth.signInAnonymously().then(response => {
            const user = firebaseAuth.currentUser
            if (user !== null) {
                console.log('Anonymous user was created' + user.uid)
            } else {
                console.log('no user was created')
            }
            console.log('response from add user ' + response)
        })
        .catch(function (error) {
            // Handle Errors here.
            const errorCode = error.code
            const errorMessage = error.message
            console.log('Error from anymous login/register ' + errorMessage)
          })
    },
    handleAuthStateChanged ({ commit, dispatch, state }) {
        console.log('firing auth state changed')
        firebaseAuth.onAuthStateChanged(function (user) {
            if (user) {
              // User is signed in.
            const userId = firebaseAuth.currentUser
            if (userId !== null) {
             console.log('Handle Auth changed User is not blank')
             console.log('Handle Auth changed User: UserID + ' + userId.uid)
              const isAnonymous = user.isAnonymous
                const uid = user.uid
                    commit('setUserDetails', {
                    name: userId.displayName,
                    email: userId.email,
                    online: true,
                    userId: userId.uid
                })

                // Update the database

            }
              // ...
            } else {
              // User is signed out.
              console.log('handle auth state User is signed out')
              dispatch('firebaseUpdateUser', {
                userId: state.userDetails.userId,
                updates: {
                    online: false
                }
                })
              commit('setUserDetails', {})
            }
            // ...
          })
    },
    loginUser ({ commit, dispatch }, payload) {

        commit('setUserDetails', {
            name: payload.userDetails.name,
            email: payload.userDetails.email,
            online: true,
            userId: payload.userDetails.userid
        })

        // Update the RTDB
        dispatch('firebaseUpdateUser', {
            userId: payload.userid,
            updates: {
                online: true
            }
        })

    },
    // eslint-disable-next-line no-empty-pattern
    firebaseUpdateUser ({}, payload) {
            const user = firebaseAuth.currentUser
            if (user !== null) {

            console.log('firebaseUpdateUser: ' + user.uid)

            if (user.uid) {
                firebaseDb.ref('users/' + user.uid).update(payload.updates)
                console.log('UserId ' + user.uid)
            }
        }
    },
    logoutUser ({ commit, dispatch }) {

        const user = firebaseAuth.currentUser
        if (user !== null) {

            // Update the RTDB
            dispatch('firebaseUpdateUser', {
                userId: user.uid,
                updates: {
                    online: false
                }
            })
        }

        firebaseAuth.signOut()
        commit('removeUserDetails')
        commit('removeUserPermissions')
        // console.log('LogoutUser from store')
    },
    clearUserState ({ commit }) {
        firebaseAuth.signOut()
        commit('removeUserDetails')
        commit('removeUserPermissions')
    },
    getBasketFromState ({ state }) {
        console.log('get basket from state')
        return state.basket
    },
    upgradeUser ({ dispatch }, payload) {
        // (Anonymous user is signed in at that point.)
        // 1. Create the email and password credential, to upgrade the
        // anonymous user.
        const credential = firebaseRoot.auth.EmailAuthProvider.credential(payload.email, payload.password)

            if (firebaseAuth.currentUser !== null) {
            // 2. Links the credential to the currently signed in user
            // (the anonymous user).
            firebaseAuth.currentUser.linkWithCredential(credential).then(function (user) {
            console.log('Anonymous account successfully upgraded', user)
            }, function (error) {
            console.log('Error upgrading anonymous account', error)
            })
            dispatch('handleNewUser', {
                name: payload.name,
                email: payload.email
            })
        }
        
    },
    handleNewUser ({ commit }, payload) {
        const user = firebaseAuth.currentUser
            if (user != null) {
                const userId = user.uid
                // Add to the dUsers table 
                 firebaseDb.ref('users/' + userId).set({
                     name: payload.name,
                     email: payload.email,
                     online: true,
                     userrole: 3
                 })

                 // Clear any previous users from State
                 commit('removeUserDetails')
                 // Clear any previous user permissions from State
                 commit('removeUserPermissions')
                 commit('setUserDetails', {
                    name: payload.name,
                    email: payload.email,
                    online: true,
                    userId: userId
                })

                // Set permissions
                firebaseDb.ref('users/' + userId).once('value', snapshot => {

                    const userDetails = snapshot.val()
                    let roleRefDetailsRolePerm = ''
    
                    // Get User Role Details
                    firebaseDb.ref('userroles/' + userDetails.userrole).once('value', snapshot => {
                      const roleRefDetails = snapshot.val()
                      const roleName = roleRefDetails.name
                      roleRefDetailsRolePerm = roleRefDetails.roleperm

                      console.log('roleRefDetailsRolePerm ' + roleRefDetailsRolePerm)
    
                        firebaseDb.ref('roleperm/' + roleRefDetailsRolePerm).once('value', snapshot => {
                           const userPermissionsDetails = snapshot.val()
                            console.log('role perm name internal: ' + snapshot.val().name)

                            const userPermission = {
                                userId: '',
                                userRole: '',
                                userRoleId: '',
                                userRolePermissionsDescription: ''
                            }

                            userPermission.userId = userId
                            userPermission.userRole = roleName
                            userPermission.userRoleId = userDetails.userrole
                            userPermission.userRolePermissionsDescription = snapshot.val().name

                            const payload = {
                                userId: userPermission.userId,
                                userPermission: userPermission
                            }

                            // set the permission
                            commit('setUserPermissions', payload)
                        })
                    })              
                })
            }
    },
    registerUser ({ commit }, payload) {
        
        firebaseAuth.createUserWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
            console.log('payload email ' + payload.email)
            console.log('payload password ' + payload.password)
            console.log('payload name ' + payload.name)
            const user = firebaseAuth.currentUser
                if (user != null) {
                const userId = user.uid
                // Add to the dUsers table 
                 firebaseDb.ref('users/' + userId).set({
                     name: payload.name,
                     email: payload.email,
                     online: true,
                     userrole: 3
                 })

                 // Clear any previous users from State
                 commit('removeUserDetails')
                 // Clear any previous user permissions from State
                 commit('removeUserPermissions')
                 commit('setUserDetails', {
                    name: payload.name,
                    email: payload.email,
                    online: true,
                    userId: userId
                })

                // Set permissions
                firebaseDb.ref('users/' + userId).once('value', snapshot => {

                    const userDetails = snapshot.val()
                    let roleRefDetailsRolePerm = ''
    
                    // Get User Role Details
                    firebaseDb.ref('userroles/' + userDetails.userrole).once('value', snapshot => {
                      const roleRefDetails = snapshot.val()
                      const roleName = roleRefDetails.name
                      roleRefDetailsRolePerm = roleRefDetails.roleperm

                      console.log('roleRefDetailsRolePerm ' + roleRefDetailsRolePerm)
    
                        firebaseDb.ref('roleperm/' + roleRefDetailsRolePerm).once('value', snapshot => {
                           const userPermissionsDetails = snapshot.val()
                            console.log('role perm name internal: ' + snapshot.val().name)

                            const userPermission = {
                                userId: '',
                                userRole: '',
                                userRoleId: '',
                                userRolePermissionsDescription: ''
                            }

                            userPermission.userId = userId
                            userPermission.userRole = roleName
                            userPermission.userRoleId = userDetails.userrole
                            userPermission.userRolePermissionsDescription = snapshot.val().name

                            const payload = {
                                userId: userPermission.userId,
                                userPermission: userPermission
                            }

                            // set the permission
                            commit('setUserPermissions', payload)
                        })
                    })              
                })
            }
        })
        .catch(function (error) {

        const errorCode = error.code
        const errorMessage = error.message
            if (errorCode === 'auth/weak-password') {
                alert('The password is too weak.')
            } else {
                alert(errorMessage)
            }
        console.log(error)
        })
    },
    registerUserOtherSource ({ commit, dispatch }, payload) {
        const user = firebaseAuth.currentUser
                if (user != null) {
                const userId = user.uid
                // Add to the RTDB Users table 
                 firebaseDb.ref('users/' + userId).set({
                     name: payload.name,
                     email: payload.email,
                     online: true,
                     userrole: 3
                 })

                 // Add to the Firestore Users table
                 
                 // Clear any previous users from State
                 commit('removeUserDetails')
                 // Clear any previous user permissions from State
                 commit('removeUserPermissions')
                 commit('setUserDetails', {
                    name: payload.name,
                    email: payload.email,
                    online: true,
                    userId: userId
                })

                // Update the RTDB
                dispatch('firebaseUpdateUser', {
                    userId: payload.userid,
                    updates: {
                        online: true
                    }
                })

                // Set permissions
                firebaseDb.ref('users/' + userId).once('value', snapshot => {

                    const userDetails = snapshot.val()
                    let roleRefDetailsRolePerm = ''
    
                    // Get User Role Details
                    firebaseDb.ref('userroles/' + userDetails.userrole).once('value', snapshot => {
                      const roleRefDetails = snapshot.val()
                      const roleName = roleRefDetails.name
                      roleRefDetailsRolePerm = roleRefDetails.roleperm

                      console.log('roleRefDetailsRolePerm ' + roleRefDetailsRolePerm)
    
                        firebaseDb.ref('roleperm/' + roleRefDetailsRolePerm).once('value', snapshot => {
                           const userPermissionsDetails = snapshot.val()
                            console.log('role perm name internal: ' + snapshot.val().name)

                            const userPermission = {
                                userId: '',
                                userRole: '',
                                userRoleId: '',
                                userRolePermissionsDescription: ''
                            }

                            userPermission.userId = userId
                            userPermission.userRole = roleName
                            userPermission.userRoleId = userDetails.userrole
                            userPermission.userRolePermissionsDescription = snapshot.val().name

                            const payload = {
                                userId: userPermission.userId,
                                userPermission: userPermission
                            }

                            // set the permission
                            commit('setUserPermissions', payload)
                        })
                    })              
                })
            }
    }
}
const getters = {
    productsFromState: state => {
        console.log('Fired Getter')
        return state.products
    },
    productsCount: state => {
        console.log('products Count fired')
        console.log('length ' + state.products.length)
        return state.products.length
    },
    getShoppingCartFromState: state => {
        console.log('get cart from state')
        return state.basket
    },
    loggedInUserDetails: state => {

        return state.userDetails
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
