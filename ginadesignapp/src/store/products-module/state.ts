export const PRODUCTS = 'data'

export const PAGINATION = 'pagination'

export interface ExampleStateInterface {
  prop: boolean;
}

// const state: ExampleStateInterface = {
//   prop: false
// }

// export default state
export default {
  [PRODUCTS]: [],
  [PAGINATION]: {
    page: 1,
    limit: 10,
    totalPages: 0
  }
}
