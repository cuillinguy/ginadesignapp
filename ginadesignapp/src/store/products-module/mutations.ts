import { MutationTree } from 'vuex'
import state, { ExampleStateInterface } from './state'
export const SET_PAGINATION = 'set_pagination'
export const SET_DATA = 'set_data'

const mutation: MutationTree<ExampleStateInterface> = {
  someMutation (/* state: ExampleStateInterface */) {
    // your code

  }
}

// export default mutation
export default {
  [SET_PAGINATION] (state, pagination) {
    state.pagination = pagination
  },

  [SET_DATA] (state, data) {
    state.data = data
  }
}
