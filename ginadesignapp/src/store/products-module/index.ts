import { Module } from 'vuex'
import { StoreInterface } from '../index'
import state from './state'
// import actions from './actions'
import getters from './getters'
import mutations from './mutations'

export const PRODUCTS_MODULE = 'products'

export default {
  namespaced: true,
  state
}
 export * from './state'
