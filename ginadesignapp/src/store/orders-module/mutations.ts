import { MutationTree } from 'vuex'
import Vue from 'vue'
import { StateInterface } from './state'
import state from '../products-module/state'
import { stat } from 'fs'
// import state from '../products-module/state'

const mutation: MutationTree<StateInterface> = {
  someMutation (state: StateInterface) {
    // your code
    // state.
  },
  addOrder (state: StateInterface, payload) {
    // Fired AddOrderDetails Mutation 
    console.log('fired add order details mutation OrderID ' + payload.orderId)
     Vue.set(state.userOrders.orderDetails, payload.orderId, payload.orderDetails)  
    // state.userOrders.orderDetails.push(payload.orderDetails) 
  },
  clearOrders (state: StateInterface) {
      // state.orders.userOrders.orderDetails.length = 0
      // state.orders.userOrders.orderItems.length = 0
      // state.orders.userOrders.orderDetails. = {}
      
      const getDefaultState = () => {
        return {
            userOrders: {
              orderDetails: [],
              orderItems: []
            }
        }
      }
      Object.assign(state.userOrders, getDefaultState())
      console.log('cleared orders from state')
  }
}

export default mutation
