import { GetterTree } from 'vuex'
import { StoreInterface } from '../index'
import { StateInterface } from './state'

const getters: GetterTree<StateInterface, StoreInterface> = {
  someAction (/* context */) {
    // your code
  }
}

export default getters
