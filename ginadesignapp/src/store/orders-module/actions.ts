import { ActionTree } from 'vuex'
import { StoreInterface } from '../index'
import { firestoreDb, firebaseAuth, firebaseDb } from 'boot/firebase'
import state, { StateInterface } from './state'

const actions: ActionTree<StateInterface, StoreInterface> = {
  someAction (/* context */) {
    // your code
  },
  getUserOrders ({ commit }, payload) {
    // clear previous order state
    // commit('clearOrders')
    console.log('fired getuserorders')
    console.log('userdetails userid ' + payload.userDetails.userId)
    const userId = payload.userDetails.userId
      const ordersRef = firestoreDb
      .collection('Orders')
      // eslint-disable-next-line quotes
      .where("UserId", "==", payload.userDetails.userId)
      .orderBy('updated_at')
      .get()
      .then(snapshot => {
        snapshot.forEach((doc) => {

          const orderDetails = {
              OrderId: '',
              AddressFirstLine: '',
              AddressSecondLine: '',
              AddressThirdLine: '',
              PostCode: '',
              ContactEmail: '',
              ContactPhone: '',
              DeliveryNote: ''
          }

          orderDetails.AddressFirstLine = doc.data().AddressFirstLine
          orderDetails.AddressSecondLine = doc.data().AddressSecondLine
          orderDetails.AddressThirdLine = doc.data().AddressThirdLine
          orderDetails.PostCode = doc.data().PostCode
          orderDetails.ContactEmail = doc.data().ContactEmail
          orderDetails.ContactPhone = doc.data().ContactPhone
          const documentId = doc.id
          console.log('address first line' + orderDetails.AddressFirstLine)
          console.log('document/collection id ' + documentId)
          orderDetails.OrderId = documentId
          console.log('orderDetails orderid ' + orderDetails.OrderId)

          // Add to State
          const payload = {
            orderId: documentId,
            orderDetails: orderDetails
          }
          // Commit mutation
          commit('addOrder', payload)

        })
      })
      .catch(err => {
        console.log('Error' + err)
      })
  }
}

export default actions
