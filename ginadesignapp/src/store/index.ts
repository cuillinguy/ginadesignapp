import { store } from 'quasar/wrappers'
import firestore from './store'
import Vuex from 'vuex'
import products, { PRODUCTS_MODULE } from './products-module'
import orders, { ORDERS_MODULE } from './orders-module'
import createPersistedState from 'vuex-persistedstate'

// import example from './module-example';
// import { ExampleStateInterface } from './module-example/state'; 

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export interface StoreInterface {
  // Define your own store structure, using submodules if needed
  // example: ExampleStateInterface;
  // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.
  example: unknown;
}

export default store(function ({ Vue }) {
  Vue.use(Vuex)

  const Store = new Vuex.Store<StoreInterface>({
    modules: {
      // example
      firestore,
      [PRODUCTS_MODULE]: products,
      [ORDERS_MODULE]: orders
    },
    plugins: [createPersistedState()],
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: !!process.env.DEV
  })

  return Store
})
